$(document).ready(async () => {
    // initialize submitButton with false state
    $("#submitButton").css({"background": "gray", "cursor" : "not-allowed"})

    // Check that all required form fields are filled in as they change
    $("#inputForm :input").on('keyup change paste', () => {
        let valid = true;
        if( $('input[name="saveEmailForPia"]:checked').val() === 'no' &&
            $('input[name="saveEmailForShip"]:checked').val() === 'no'){
                $('input[name="email"').prop("required", false)
                $('input[name="email"').prop("disabled", true)
        }else{
            $('input[name="email"').prop("required", true)
            $('input[name="email"').prop("disabled", false)
        }
        
        $('[required]').each(function() {
            const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g;
            if ($(this).is(':invalid')
            || !$(this).val() 
            || ($(this).attr("type") === 'email' 
            && !re.test($(this).val())))
                valid = false
        })
        if (!valid) {
            $("#submitButton").css({"background": "gray", "cursor" : "not-allowed"})
        } else {
            $("#submitButton").css({"background": "green", "cursor" : "pointer"})
        }
    })
    $("#inputForm :input").change()

    
    // password toggle button 
    $("#pwToggle").click(() => {
        if($('#pwInput').attr("type") === "password"){
            $('#pwInput').attr("type", "text")
            $('#pwToggle').children().attr("class", "fa fa-eye-slash")
        }
        else{
            $('#pwToggle').children().attr("class", "fa fa-eye")
            $('#pwInput').attr("type", "password")
        }
    })
    
    // get the alerts
    const res = await $.post('/alerts')
    if (res?.errors){
        // build the alerts
        res?.errors.forEach((e) => {
            console.log(new Date(), "error: ", e.msg)
            let alertBox = $("<div>")
            alertBox.addClass("alert")
            alertBox.html(e.msg)
            let alertBtn = $("<button>")
            alertBtn.addClass("closeAlert")
            alertBtn.html("&times;")
            alertBtn.click((e) => $(e.target).closest(".alert").hide(150))
            alertBox.append(alertBtn)
        $("#inputForm").before(alertBox)
        alertBox.hide().show(150)
        })
    }
})