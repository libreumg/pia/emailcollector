#!/bin/bash

V=`more piaemailcollector/DEBIAN/control | grep Version: | sed 's/Version: //g'`

dpkg -b piaemailcollector piaemailcollector_$V.deb
